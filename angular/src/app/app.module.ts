import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {AppAddEditDialogComponent, AppComponent} from './app.component';
import {NZ_I18N} from 'ng-zorro-antd/i18n';
import {en_US} from 'ng-zorro-antd/i18n';
import {CommonModule, registerLocaleData} from '@angular/common';
import en from '@angular/common/locales/en';
import {FormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {NzButtonModule} from 'ng-zorro-antd/button';
import {NzMenuModule} from 'ng-zorro-antd/menu';
import {MatDialogModule} from "@angular/material/dialog";
import {NzFormModule} from "ng-zorro-antd/form";
import {NzSelectModule} from "ng-zorro-antd/select";
import {NzInputModule} from 'ng-zorro-antd/input';
import {NzCardModule} from 'ng-zorro-antd/card';
import {NzPopconfirmModule} from 'ng-zorro-antd/popconfirm';
import {NzEmptyModule} from 'ng-zorro-antd/empty';
import {NgxPaginationModule} from "ngx-pagination";
import {NzIconModule} from "ng-zorro-antd/icon";

registerLocaleData(en);

@NgModule({
  declarations: [
    AppComponent,
    AppAddEditDialogComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    NzButtonModule,
    NzMenuModule,
    MatDialogModule,
    NzFormModule,
    NzSelectModule,
    NzInputModule,
    NzCardModule,
    NzPopconfirmModule,
    NzEmptyModule,
    NgxPaginationModule,
    NzIconModule
  ],
  providers: [{provide: NZ_I18N, useValue: en_US}],
  bootstrap: [AppComponent]
})
export class AppModule {
}
