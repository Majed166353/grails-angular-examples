import {Component, Inject, OnInit} from '@angular/core';
import {DataService} from "../services/data.service";
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import Swal from 'sweetalert2'
import {CommonService} from "../services/common.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  collections = [];
  totalItems = 0;
  pageOffset = 1;
  itemsPerPage = 10;
  sortColumn = "id";
  dataOrderBy = true;
  searchValues = "";

  constructor(private dataService: DataService, public dialog: MatDialog,
              private common: CommonService) {
    this.getCollections();
    this.common.aClickedEvent.subscribe((data: string) => {
      this.getCollections();
    });
  }

  getCollections() {
    let orderBy: string;
    if (this.dataOrderBy === true) {
      orderBy = 'DESC';
    } else {
      orderBy = 'ASC';
    }

    const postData = {
      'itemsPerPage': Number(this.itemsPerPage),
      'pageOffset': this.pageOffset,
      'columnToSort': this.sortColumn,
      'orderBy': orderBy,
      'searchBy': this.searchValues,
    }

    this.dataService.post(postData, 'grailsCRUD/index')
      .subscribe(data => {
        this.collections = data.list;
        this.totalItems = data.totalCount;
      });
  }

  create() {
    this.dialog.open(AppAddEditDialogComponent, {
      width: '800px',
      data: {
        firstName: '',
        lastName: '',
        salary: '',
        operation: 'Create'
      }
    });
  }

  edit(collection) {
    this.dialog.open(AppAddEditDialogComponent, {
      width: '800px',
      data: {
        id: collection.id,
        firstName: collection.firstName,
        lastName: collection.lastName,
        salary: collection.salary,
        operation: 'Update'
      }
    });
  }

  cancel(): void {
  }

  confirm(id): void {
    this.dataService.softDelete(id, 'grailsCRUD/delete')
      .subscribe(data => {
        if (data.status == 200) {
          Swal.fire({
            position: 'top-end',
            icon: 'success',
            title: data.message,
            showConfirmButton: false,
            timer: 1500,
            width: '320px',
            heightAuto: false,
          })
          this.common.AClicked('component clicked');
        }
      });
  }

  pageChange(newPage: number) {
    this.pageOffset = newPage;
    this.getCollections();
  }

  sort(column: string) {
    if (this.sortColumn === column) {
      this.dataOrderBy = !this.dataOrderBy;
    } else {
      this.sortColumn = column;
    }
    this.getCollections();
  }

  itemsOnChange(event) {
    this.pageOffset = 1;
    this.itemsPerPage = event;
    this.getCollections();
  }

  getSL(i) {
    return (Number(this.itemsPerPage) * (Number(this.pageOffset) - 1)) + i;
  }

  onSearch(value: string) {
    this.searchValues = value;
    this.pageOffset = 1;
    this.itemsPerPage = 10;
    this.dataOrderBy = true;
    this.getCollections();
  }
}

@Component({
  templateUrl: './app-add-edit-dialog.component.html',
  styleUrls: ['./app.component.css']
})
export class AppAddEditDialogComponent {
  constructor(private dataService: DataService, @Inject(MAT_DIALOG_DATA) public data: any,
              public dialogRef: MatDialogRef<AppAddEditDialogComponent>,
              private common: CommonService
  ) {
  }

  submit() {
    this.dataService.post(this.data, 'grailsCRUD/save')
      .subscribe(data => {
        if (data.status == 200) {
          Swal.fire({
            position: 'top-end',
            icon: 'success',
            title: data.message,
            showConfirmButton: false,
            timer: 1500,
            width: '320px',
            heightAuto: false,
          })
          this.common.AClicked('component clicked');
          this.dialogRef.close();
        }
      });
  }
}


