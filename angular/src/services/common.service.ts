import { Component,Injectable, Inject } from '@angular/core';
import { Input, Output, EventEmitter } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import {environment} from "../environments/environment";

const CSV_TYPE = 'text/csv';
const CSV_EXTENSION = '.csv';
export interface DialogData {
  baseURL: string,
  image: string
}

@Injectable({
    providedIn: 'root'
})
export class CommonService {
//
//     mycookie;
    aClickedEvent: EventEmitter<any> = new EventEmitter<string>();
//     aClickedEventLost: EventEmitter<any> = new EventEmitter<string>();
//     onMainEvent: EventEmitter<any> = new EventEmitter();
//     onBufferEvent: EventEmitter<any> = new EventEmitter();
//     rootUrl = environment.API_URL;
//     cookieName = 'something';
//     showPrintHeader = true;
//     today: Date = new Date();
//     rolelist = [];
//     cook;
//     // tosty options
//     position = 'top-right';
//
//
//     // OTP
//     countdowntime = '00 : 00 : 00';
//     countDownInterval;
//     ones = ['', 'one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine'];
//     tens = ['', '', 'twenty', 'thirty', 'forty', 'fifty', 'sixty', 'seventy', 'eighty', 'ninety'];
//     teens = ['ten', 'eleven', 'twelve', 'thirteen', 'fourteen', 'fifteen', 'sixteen', 'seventeen', 'eighteen', 'nineteen'];
//
//
//     // Frontend Image Sizes
//     public ecomCategoryBannerSizes: any = {
//       width: 1190,
//       height: 300
//     };
//     public ecomShopLogoSizes: any = {
//       width: 1190,
//       height: 300
//     };
//     public ecomShopThumbnailSizes: any = {
//       width: 1190,
//       height: 300
//     };
//     public ecomShopBannerSizes: any = {
//       width: 1190,
//       height: 300
//     };
//
//     constructor(private auth: AuthService, private toastyService: ToastyService,public dialog: MatDialog ) {
//         this.cook = this.auth.getCookie('QGluZiNpbmZvdGVjaCM');
//         if (this.cook) {
//             this.mycookie = JSON.parse(atob(this.cook));
//         } else {
//             this.mycookie = '';
//         }
//     }
//
//     convert(num): string {
//       if (num === 0) {return 'zero'; }
//       else {return this.convert_millions(num); }
//     }
//
//
//     convert_tens(num): string {
//       if (num < 10) { return this.ones[num]; }
//       else if (num >= 10 && num < 20) { return this.teens[num - 10]; }
//       else {
//         return this.tens[Math.floor(num / 10)] + ' ' + this.ones[num % 10];
//       }
//     }
//
//     convert_hundreds(num): string {
//       if (num > 99) {
//         return this.ones[Math.floor(num / 100)] + ' hundred ' + this.convert_tens(num % 100);
//       } else {
//         return this.convert_tens(num);
//       }
//     }
//
//     convert_thousands(num): string {
//       if (num >= 1000) {
//         return this.convert_hundreds(Math.floor(num / 1000)) + ' thousand ' + this.convert_hundreds(num % 1000);
//       } else {
//         return this.convert_hundreds(num);
//       }
//     }
//
//     convert_millions(num): string {
//       if (num >= 1000000) {
//         return this.convert_millions(Math.floor(num / 1000000)) + ' million ' + this.convert_thousands(num % 1000000);
//       } else {
//         return this.convert_thousands(num);
//       }
//     }
//
//     invoiceDRamount(entry){
//       var amount = 0;
//       entry.forEach(element => {
//         if (element.entry_type == 'D'){
//           amount += Number(element.amount);
//         }
//       });
//       return Number(amount);
//     }
//
//     invoiceCRamount(entry){
//       var amount = 0;
//       entry.forEach(element => {
//         if (element.entry_type == 'C'){
//           if (element.payable == 'vat'){
//             if (element.payable_type == 1){
//               amount += Number(-1 * element.amount);
//             }else{
//               amount += Number(element.amount);
//             }
//           }else{
//             amount += Number(element.amount);
//           }
//         }
//       });
//       return Number(-1 * amount);
//     }
//
//
//     permission(component, action): number {
//         let permissions = localStorage.getItem('n_QGluZiNpbmZvdGVjaCM');
//         if (permissions !== undefined) {
//             let menu = JSON.parse(atob(permissions));
//             let access = 0;
//             menu.forEach((me, mi) => {
//                  if (('permited_menus' in me)) {
//                 const permited_menus = me.permited_menus;
//                 permited_menus.forEach((element, i) => {
//                     let permit = element.permissions;
//                     permit.forEach((e, ei) => {
//                         if (e.component == component) {
//                             if (action === 'create') {
//                                 if (e.role_permit_create == 1) {
//                                     access = 1;
//                                 } else {
//                                     access = 0;
//                                 }
//                             } else if (action === 'view') {
//                                 if (e.role_permit_view == 1) {
//                                     access = 1;
//                                 } else {
//                                     access = 0;
//                                 }
//                             } else if (action === 'edit') {
//                                 if (e.role_permit_edit == 1) {
//                                     access = 1;
//                                 } else {
//                                     access = 0;
//                                 }
//                             } else if (action === 'delete') {
//                                 if (e.role_permit_delete == 1) {
//                                     access = 1;
//                                 } else {
//                                     access = 0;
//                                 }
//                             }
//                         } else {
//                             return 0;
//                         }
//                     });
//                 });
//                 }
//             });
//             return access;
//         } else {
//             return 0;
//         }
//     }
//
//
//
    AClicked(msg: string): void {
        this.aClickedEvent.emit(msg);
    }
//
//     getCookie(cname) {
//         const name = cname + '=';
//         const ca = document.cookie.split(';');
//         for (let i = 0; i < ca.length; i++) {
//             let c = ca[i];
//             while (c.charAt(0) === ' ') {
//                 c = c.substring(1);
//             }
//             if (c.indexOf(name) === 0) {
//                 return c.substring(name.length, c.length);
//             }
//         }
//         return '';
//     }
//
//     updateCookie(cname, cvalue, exdays): void {
//         const d = new Date();
//         d.setTime(d.getTime() + (exdays * 30 * 60 * 1000));
//         const expires = 'expires=' + d.toUTCString();
//         document.cookie = cname + '=' + cvalue + ';' + expires + ';path=/';
//     }
//
//     checkCookie(): void {
//         const cookie = this.getCookie(this.cookieName);
//         if (cookie) {
//             const copkiedata = JSON.parse(cookie);
//             this.updateCookie(this.cookieName, cookie, 1);
//         }
//     }
//
//     setCookie(cname, cvalue, exdays): void {
//         const d = new Date();
//         d.setTime(d.getTime() + (exdays * 900 * 60 * 1000));
//         const expires = 'expires=' + d.toUTCString();
//         document.cookie = cname + '=' + cvalue + ';' + expires + ';path=/';
//     }
//
//     printwindow(): boolean {
//         const allmembers = document.querySelector('.memebr_tbl').innerHTML;
//
//         const mywindow = window.open('', 'Infinity Marketing Limited', 'height=1000,width=1200');
//         // tslint:disable-next-line: max-line-length
//         mywindow.document.write('<html><head><title></title><style>table {border-collapse: collapse;width:100%;}table, table td, table th {border: 1px solid #666;padding: 6px 8px;font-size: 14px;}.forfooter::before{content:"";display:table;clear:both;}</style>');
//         mywindow.document.write('</head><body >');
//         if (this.showPrintHeader) {
//             // tslint:disable-next-line: max-line-length
//             mywindow.document.write('<div style="width:100%;display:block;"><h3 style="text-align:center;">Infinity Marketing Ltd.</h3><p style="text-align:center;">Jabbar Tower,Road no-135 House No-42, Level-6, Gulshan Avenue-1 Dhaka-1212,Bangladesh</p><h3  style="text-align:center;">Total Income Report (infinity6)</h3></div>');
//         }
//         // tslint:disable-next-line: max-line-length
//         mywindow.document.write('<div style="width:100%; display:block;"><div style=" width:70%;margin:0 auto;">' + allmembers + '</div></div>');
//
//         if (this.showPrintHeader) {
//             // tslint:disable-next-line: max-line-length
//             mywindow.document.write('<div class="forfooter" style="width:100%;display:block;margin-top:26px;"><div style="text-align:center;width:100%;margin-top:26px;"> Copyright &copy;' + this.today.getFullYear() + '</div><p style="text-align:center;">All Rights Reserved By Infinity Marketing Limited</p></div>');
//         }
//         mywindow.document.write('</body></html>');
//         mywindow.print();
//         mywindow.close();
//
//         return true;
//     }
//
//     getMonthName(month): void {
//         let monthname;
//         switch (month) {
//             case '1':
//                 monthname = 'JAN';
//                 break;
//             case '2':
//                 monthname = 'FEB';
//                 break;
//             case '3':
//                 monthname = 'MAR';
//                 break;
//             case '4':
//                 monthname = 'APR';
//                 break;
//             case '5':
//                 monthname = 'MAY';
//                 break;
//             case '6':
//                 monthname = 'JUN';
//                 break;
//             case '7':
//                 monthname = 'JUL';
//                 break;
//             case '8':
//                 monthname = 'AUG';
//                 break;
//             case '9':
//                 monthname = 'SEP';
//                 break;
//             case '10':
//                 monthname = 'OCT';
//                 break;
//             case '11':
//                 monthname = 'NOV';
//                 break;
//             case '12':
//                 monthname = 'DEC';
//                 break;
//             default:
//             // code block
//
//         }
//         return monthname;
//     }
//
//
//
//     // tslint:disable-next-line: typedef
//     mysqlDate(dateTime) {
//         const yyyy = dateTime.getFullYear().toString();
//         let mm = dateTime.getMonth() + 1;
//         if (mm < 10) {
//           mm = '0' + mm.toString();
//         } else {
//           mm = mm.toString();
//         }
//         let dd = dateTime.getDate();
//         if (dd < 10) {
//           dd = '0' + dd.toString();
//         } else {
//           dd = dd.toString();
//         }
//         let hh = dateTime.getHours();
//         if (hh < 10) {
//           hh = '0' + hh.toString();
//         } else {
//           hh = hh.toString();
//         }
//         let mmm = dateTime.getMinutes();
//         if (mmm < 10) {
//           mmm = '0' + mmm.toString();
//         } else {
//           mmm = mmm.toString();
//         }
//         let ss = dateTime.getSeconds();
//         if (ss < 10) {
//           ss = '0' + ss.toString();
//         } else {
//           ss = ss.toString();
//         }
//         return yyyy + '-' + mm + '-' + dd + ' ' + hh + ':' + mmm + ':' + ss;
//       }
//
// // tslint:disable-next-line: typedef
// // openToasty(title: string, message: string, type: string) {
// //     // tslint:disable-next-line: max-line-length
// //     const options = { title: title, msg: message, timeout: 5000, theme: 'default', position: 'top-right', type: type, closeOther: true, showClose: true};
// //     if (options.closeOther) {
// //       this.toastyService.clearAll();
// //     }
// //     this.position = options.position ? options.position : this.position;
// //     const toastOptions: ToastOptions = {
// //       title: options.title,
// //       msg: options.msg,
// //       showClose: options.showClose,
// //       timeout: options.timeout,
// //       theme: options.theme,
// //       onAdd: (toast: ToastData) => {
// //         /* added */
// //       },
// //       onRemove: (toast: ToastData) => {
// //         /* removed */
// //       }
// //     };
// //
// //     switch (options.type) {
// //       case 'default': this.toastyService.default(toastOptions); break;
// //       case 'info': this.toastyService.info(toastOptions); break;
// //       case 'success': this.toastyService.success(toastOptions); break;
// //       case 'wait': this.toastyService.wait(toastOptions); break;
// //       case 'error': this.toastyService.error(toastOptions); break;
// //       case 'warning': this.toastyService.warning(toastOptions); break;
// //     }
// //   }
//
//
//
// // Writer: Dulal
//     // Subject Our OTP
//
//      setCountDown = (endTime) => {
//         this.countdowntime = '00 : 00 : 00'
//           // calculate how many milliseconds is left to reach endTime from now
//           let secondsLeftms = endTime - Date.now();
//           // convert it to seconds
//           const secondsLeft = Math.round(secondsLeftms / 1000);
//
//           // calculate the hours, minutes and seconds
//           let hours = Math.floor(secondsLeft / 3600);
//           let minutes = Math.floor(secondsLeft / 60) - (hours * 60);
//           let seconds = secondsLeft % 60;
//
//           // adding an extra zero infront of digits if it is < 10
//           if (hours < 10) {
//             hours = Number(`0${hours}`);
//           }
//           if (minutes < 10) {
//             minutes = Number(`0${minutes}`);
//           }
//           if (seconds < 10) {
//             seconds = Number(`0${seconds}`);
//           }
//
//           // stopping the timer if the time is up
//           if (secondsLeft < 0) {
//
//             clearInterval(this.countDownInterval);
//             this.countdowntime = '00 : 00 : 00'
//           } else {
//             this.countdowntime = `${hours} : ${minutes} : ${seconds}`;
//
//           }
//
//           // set the .countdown text
//
//         };
//
//         startTime(format, timeInput) {
//          // event.preventDefault();
//           let countDownTime = timeInput;
//
//
//           if (countDownTime > 0) {
//             // check which is the format, ie the <select> element's value
//             if (format === 'hours') {
//               // convert hours to milliseconds
//               // 1 hrs = 3600000 ms (5 zeros)
//               countDownTime = countDownTime * 3600000;
//             } else if (format === 'minutes') {
//               // 1 minute = 60000 ms (4 zeros)
//               countDownTime = countDownTime * 60000;
//             } else if (format === 'seconds') {
//               // 1 seconds = 1000 ms (3 zeros)
//               countDownTime = countDownTime * 1000;
//             }
//
//           const now = Date.now();
//           // calculate the ending time
//           let endTime = now + countDownTime;
//
//           // activate the countdown at first
//            this.setCountDown(endTime);
//
//          this.countDownInterval = setInterval(() => {
//             this.setCountDown(endTime);
//           }, 1000);
//
//         }
//
//         }
//
//         resetCountDown = () => {
//           clearInterval(this.countDownInterval);
//         };
//
//
//   public generateSlug(str) {
//       if (str == null || str == '' || str == undefined) return;
//       str = str.toString().toLowerCase()
//           .replace(str, str)
//           .replace(/^-+|-+$/g, '')
//           .replace(/\s+/g, '-') // Replace spaces with -
//           .replace(/&/g, '-and-') // Replace & with 'and'
//           .replace(/\-\-+/g, '-') // Replace multiple - with single -
//           .replace(/^-+/, '') // Trim - from start of text
//           .replace(/-+$/, '') // Trim - from end of text
//
//       return str;
//   }
//
//   openImageServiceModel(baseURL, image) {
//     const dialogRef = this.dialog.open(OpenServiceImgModal, {
//       // width: '650px',
//       panelClass: 'pannel-padding-remove',
//       data: {
//         baseURL: baseURL,
//         image: image
//       }
//     });
//   }
//
//   /**
//    * Saves the file on the client's machine via FileSaver library (Not Using It, 475 Line).
//    *
//    * @param buffer The data that need to be saved.
//    * @param fileName File name to save as.
//    * @param fileType File type to save as.
//    */
//   private saveAsFile(buffer: any, fileName: string, fileType: string): void {
//     const data: Blob = new Blob([buffer], { type: fileType });
//
//     const a = document.createElement('a');
//     a.href = URL.createObjectURL(data);
//     a.download = fileName;
//     a.click();
//     //FileSaver.saveAs(data, fileName);
//   }
//
//   /**
//   * Creates an array of data to CSV. It will automatically generate a title row based on object keys.
//   *
//   * @param rows array of data to be converted to CSV.
//   * @param fileName filename to save as.
//   * @param columns array of object properties to convert to CSV. If skipped, then all object properties will be used for CSV.
//   */
//   public exportToCsv(rows: object[], fileName: string, columns?: string[]): string {
//     if (!rows || !rows.length) {
//         return;
//     }
//     const separator = ',';
//     const keys = Object.keys(rows[0]).filter(k => {
//         if (columns?.length) {
//             return columns.includes(k);
//         } else {
//             return true;
//         }
//     });
//     const csvContent =
//     keys.join(separator) +
//     '\n' +
//     rows.map(row => {
//         return keys.map(k => {
//             let cell = row[k] === null || row[k] === undefined ? '' : row[k];
//             cell = cell instanceof Date
//                 ? cell.toLocaleString()
//                 : cell.toString().replace(/"/g, '""');
//             if (cell.search(/("|,|\n)/g) >= 0) {
//                 cell = `"${cell}"`;
//             }
//             return cell;
//         }).join(separator);
//     }).join('\n');
//     this.saveAsFile(csvContent, `${fileName}${CSV_EXTENSION}`, CSV_TYPE);
//   }
//
//
// }

// @Component({
//   selector: 'open-service-img-modal',
//   templateUrl: './open-service-img-modal.html',
//   styles: ['.mat-dialog-container {padding:0 !important;}']
// })
// export class OpenServiceImgModal {
//   constructor(public dialogRef: MatDialogRef<OpenServiceImgModal>,
//     @Inject(MAT_DIALOG_DATA) public data: DialogData, public dialog: MatDialog) { }
//
//   onNoClick(): void {
//     console.log('di')
//   }
//
}
