package grails

class GrailsCRUD {

    String firstName
    String lastName
    double salary
    Date createdAt
    Date updatedAt
    Date deletedAt


    static constraints = {
        createdAt nullable: true
        updatedAt nullable: true
        deletedAt nullable: true
    }

    static mapping = {
    }
}
