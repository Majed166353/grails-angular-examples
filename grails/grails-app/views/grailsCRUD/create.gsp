<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="layout" content="main"/>
    <title>Document</title>
</head>

<body>
<div class="container">
<g:form controller="grailsCRUD" action="save">
    <div class="form-group">
        <label for="firstName">First Name</label>
        <input name="firstName" type="text" class="form-control" id="firstName"
               placeholder="First Name">
    </div>

    <div class="form-group">
        <label for="lastName">Last Name</label>
        <input name="lastName" type="text" class="form-control" id="lastName" placeholder="Last Name">
    </div>

    <div class="form-group">
        <label for="salary">Salary</label>
        <input name="salary" type="number" class="form-control" id="salary" placeholder="Salary">
    </div>
    <button class="btn btn-primary btn-sm" type="submit">Create</button>
    </div>
</g:form>
</div>
</body>
</html>



%{--<!DOCTYPE html>--}%
%{--<html>--}%
%{--    <head>--}%
%{--        <meta name="layout" content="main" />--}%
%{--        <g:set var="entityName" value="${message(code: 'grailsCRUD.label', default: 'GrailsCRUD')}" />--}%
%{--        <title><g:message code="default.create.label" args="[entityName]" /></title>--}%
%{--    </head>--}%
%{--    <body>--}%
%{--        <a href="#create-grailsCRUD" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>--}%
%{--        <div class="nav" role="navigation">--}%
%{--            <ul>--}%
%{--                <li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>--}%
%{--                <li><g:link class="list" action="index"><g:message code="default.list.label" args="[entityName]" /></g:link></li>--}%
%{--            </ul>--}%
%{--        </div>--}%
%{--        <div id="create-grailsCRUD" class="content scaffold-create" role="main">--}%
%{--            <h1><g:message code="default.create.label" args="[entityName]" /></h1>--}%
%{--            <g:if test="${flash.message}">--}%
%{--            <div class="message" role="status">${flash.message}</div>--}%
%{--            </g:if>--}%
%{--            <g:hasErrors bean="${this.grailsCRUD}">--}%
%{--            <ul class="errors" role="alert">--}%
%{--                <g:eachError bean="${this.grailsCRUD}" var="error">--}%
%{--                <li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>--}%
%{--                </g:eachError>--}%
%{--            </ul>--}%
%{--            </g:hasErrors>--}%
%{--            <g:form resource="${this.grailsCRUD}" method="POST">--}%
%{--                <fieldset class="form">--}%
%{--                    <f:all bean="grailsCRUD"/>--}%
%{--                </fieldset>--}%
%{--                <fieldset class="buttons">--}%
%{--                    <g:submitButton name="create" class="save" value="${message(code: 'default.button.create.label', default: 'Create')}" />--}%
%{--                </fieldset>--}%
%{--            </g:form>--}%
%{--        </div>--}%
%{--    </body>--}%
%{--</html>--}%
