<%@ page import="grails.GrailsCRUDController" %>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="layout" content="main"/>
    <title>Document</title>
</head>

<body>
<div class="container">
    <g:if test="${flash.message}">
        <button type="button" class="close" data-dismiss="alert">x</button>
        <div class="alert alert-success" role="alert" aria-hidden="true">${flash.message}</div>
    </g:if>
    <table class="table">
        <g:link controller="grailsCRUD" action="create" class="btn btn-sm btn-primary mb-2 mt-2">+ Create</g:link>
        <thead class="thead-dark">
        <tr>
            <th scope="col">#</th>
            <th scope="col">First</th>
            <th scope="col">Last</th>
            <th scope="col">Salary</th>
            <th scope="col">Action</th>
        </tr>
        </thead>
        <tbody>
        <g:each var="list" in="${grailsCRUD}" status="i">
            <tr>
                <th scope="row">${i + 1}</th>
                <td>${list.getFirstName()}</td>
                <td>${list.getLastName()}</td>
                <td>${list.getSalary()}</td>
                <td><g:link controller="grailsCRUD" action="edit" id="${list.getId()}"><i class="fas fa-edit text-info edit"></i></g:link> &emsp;|&emsp; <g:link controller="grailsCRUD" action="delete" id="${list.getId()}"><i class="fas fa-trash text-danger delete"></i></g:link></td>
            </tr>
        </g:each>
        </tbody>
    </table>
</div>

<!-- Button trigger modal -->
<!-- Modal -->
%{--<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"--}%
%{--     aria-hidden="true">--}%
%{--    <div class="modal-dialog" role="document">--}%
%{--        <div class="modal-content">--}%
%{--            <div class="modal-header">--}%
%{--                <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>--}%
%{--                <button type="button" class="close" data-dismiss="modal" aria-label="Close">--}%
%{--                    <span aria-hidden="true">&times;</span>--}%
%{--                </button>--}%
%{--            </div>--}%

%{--        <div class="modal-body">--}%
%{--            <g:form>--}%
%{--                <div class="form-group">--}%
%{--                    <label for="firstName">First Name</label>--}%
%{--                    <input type="hidden" id="id">--}%
%{--                    <input type="text" class="form-control" id="firstName" aria-describedby="emailHelp"--}%
%{--                           placeholder="First Name">--}%
%{--                    <small id="emailHelp"--}%
%{--                           class="form-text text-muted">We'll never share your email with anyone else.</small>--}%
%{--                </div>--}%

%{--                <div class="form-group">--}%
%{--                    <label for="lastName">Last Name</label>--}%
%{--                    <input type="text" class="form-control" id="lastName" placeholder="Last Name">--}%
%{--                </div>--}%

%{--                <div class="form-group">--}%
%{--                    <label for="salary">Salary</label>--}%
%{--                    <input type="number" class="form-control" id="salary" placeholder="Salary">--}%
%{--                </div>--}%
%{--                </div>--}%

%{--                <div class="modal-footer">--}%
%{--                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>--}%
%{--                    <button id="submit" type="button" class="btn btn-primary">Save changes</button>--}%
%{--                </div>--}%
%{--            </g:form>--}%
%{--        </div>--}%
%{--    </div>--}%
%{--</div>--}%
%{--<script>--}%
%{--    $(document).ready(function () {--}%
%{--        var id;--}%
%{--        var firstName;--}%
%{--        var lastName;--}%
%{--        var salary;--}%

%{--        $(".edit").click(function () {--}%
%{--            id = $(this).data('id');--}%
%{--            firstName = $(this).data('first');--}%
%{--            lastName = $(this).data('last');--}%
%{--            salary = $(this).data('salary');--}%
%{--            $('#exampleModal').modal('show');--}%
%{--            // $('#id').val(id);--}%
%{--            $('#firstName').val(firstName);--}%
%{--            $('#lastName').val(lastName);--}%
%{--            $('#salary').val(salary);--}%
%{--        });--}%

%{--        $('#submit').click(function () {--}%
%{--            var URL = "${createLink(controller:'GrailsCRUD', action:'update')}/"+id;--}%
%{--            let first =  $('#firstName').val();--}%
%{--            let last =  $('#lastName').val();--}%
%{--            let salary =  $('#salary').val();--}%
%{--            var formData = new FormData();--}%
%{--            formData.append('first_name', first);--}%
%{--            formData.append('last_name', last);--}%
%{--            formData.append('salary', salary);--}%
%{--            $.ajax({--}%
%{--                method: 'PUT',--}%
%{--                processData: false,--}%
%{--                contentType: false,--}%
%{--                data: formData,--}%
%{--                enctype: 'multipart/form-data',--}%
%{--                url: URL,--}%
%{--                success: function (resp) {--}%
%{--                    $('#exampleModal').modal('hide');--}%
%{--                    alert("Data Updated");--}%
%{--                }--}%
%{--            });--}%
%{--        });--}%

%{--        $('.delete').click(function () {--}%
%{--            let id = $(this).data('id');--}%
%{--            var URL = "${createLink(controller:'GrailsCRUD', action:'delete')}/" + id;--}%
%{--            $.ajax({--}%
%{--                url: URL,--}%
%{--                type: 'delete',--}%
%{--                success: function (resp) {--}%
%{--                    alert("Deleted!");--}%
%{--                    location.reload();--}%
%{--                }--}%
%{--            });--}%
%{--        });--}%
%{--    });--}%
%{--</script>--}%
</body>
</html>
