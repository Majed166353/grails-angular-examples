package grails

import grails.converters.JSON
import org.springframework.web.bind.annotation.RequestBody
import javax.websocket.server.PathParam

import static org.springframework.http.HttpStatus.NOT_FOUND

class GrailsCRUDController {
    GrailsCRUDService grailsCRUDService
    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index() {
//        List<GrailsCRUD> list = GrailsCRUD.where{(deletedAt == null)}.list([max: request.JSON?.itemsPerPage, offset: (request.JSON?.pageOffset - 1) * request.JSON?.itemsPerPage, sort: request.JSON?.columnToSort, order: request.JSON?.orderBy]);
        List<GrailsCRUD> list = GrailsCRUD.findAll("from GrailsCRUD as g " + "where g.deletedAt is null and (g.firstName like :search or g.lastName like :search or g.salary like :search) order by :orderBy ",
                [orderBy: request.JSON?.columnToSort,
                 max    : request.JSON?.itemsPerPage,
                 offset : (request.JSON?.pageOffset - 1) * request.JSON?.itemsPerPage,
                 search : "%" + request.JSON?.searchBy + "%"])
        render text: [list: list, status: 200, message: 'saved!', 'totalCount': grailsCRUDService.count()] as JSON
    }


    def save(@RequestBody GrailsCRUD crud) {
        if (crud.getId()) {
            GrailsCRUD grailsCrud = GrailsCRUD.findById(crud.getId())
            grailsCrud.setFirstName(crud.getFirstName())
            grailsCrud.setLastName(crud.getLastName())
            grailsCrud.setSalary(crud.getSalary())
            grailsCrud.setUpdatedAt(new Date())
            grailsCRUDService.save(grailsCrud)
            render text: [status: 200, message: 'Employee Updated!'] as JSON
        } else {
            GrailsCRUD grailsCRUD = new GrailsCRUD()
            grailsCRUD.setFirstName(crud.getFirstName())
            grailsCRUD.setLastName(crud.getLastName())
            grailsCRUD.setSalary(crud.getSalary())
            grailsCRUD.setCreatedAt(new Date())
            grailsCRUD.save()
            render text: [status: 200, message: 'Employee Saved!'] as JSON
        }
    }

    def delete(@PathParam Long id) {
        GrailsCRUD grailsCrud = GrailsCRUD.findById(id)
        grailsCrud.setDeletedAt(new Date())
        grailsCRUDService.save(grailsCrud)
        render text: [status: 200, message: 'Employee Deleted!'] as JSON
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'grailsCRUD.label', default: 'GrailsCRUD'), params.id])
                redirect action: "index", method: "GET"
            }
            '*' { render status: NOT_FOUND }
        }
    }
}
