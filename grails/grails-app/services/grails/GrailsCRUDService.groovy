package grails

import grails.gorm.services.Service

@Service(GrailsCRUD)
interface GrailsCRUDService {

    GrailsCRUD get(Serializable id)

    List<GrailsCRUD> list(Map args)

    Long count()

    void delete(Serializable id)

    GrailsCRUD save(GrailsCRUD grailsCRUD)
}