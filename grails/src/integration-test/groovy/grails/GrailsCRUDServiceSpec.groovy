package grails

import grails.testing.mixin.integration.Integration
import grails.gorm.transactions.Rollback
import spock.lang.Specification
import org.hibernate.SessionFactory

@Integration
@Rollback
class GrailsCRUDServiceSpec extends Specification {

    GrailsCRUDService grailsCRUDService
    SessionFactory sessionFactory

    private Long setupData() {
        // TODO: Populate valid domain instances and return a valid ID
        //new GrailsCRUD(...).save(flush: true, failOnError: true)
        //new GrailsCRUD(...).save(flush: true, failOnError: true)
        //GrailsCRUD grailsCRUD = new GrailsCRUD(...).save(flush: true, failOnError: true)
        //new GrailsCRUD(...).save(flush: true, failOnError: true)
        //new GrailsCRUD(...).save(flush: true, failOnError: true)
        assert false, "TODO: Provide a setupData() implementation for this generated test suite"
        //grailsCRUD.id
    }

    void "test get"() {
        setupData()

        expect:
        grailsCRUDService.get(1) != null
    }

    void "test list"() {
        setupData()

        when:
        List<GrailsCRUD> grailsCRUDList = grailsCRUDService.list(max: 2, offset: 2)

        then:
        grailsCRUDList.size() == 2
        assert false, "TODO: Verify the correct instances are returned"
    }

    void "test count"() {
        setupData()

        expect:
        grailsCRUDService.count() == 5
    }

    void "test delete"() {
        Long grailsCRUDId = setupData()

        expect:
        grailsCRUDService.count() == 5

        when:
        grailsCRUDService.delete(grailsCRUDId)
        sessionFactory.currentSession.flush()

        then:
        grailsCRUDService.count() == 4
    }

    void "test save"() {
        when:
        assert false, "TODO: Provide a valid instance to save"
        GrailsCRUD grailsCRUD = new GrailsCRUD()
        grailsCRUDService.save(grailsCRUD)

        then:
        grailsCRUD.id != null
    }
}
